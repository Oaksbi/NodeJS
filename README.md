# **Node Js Microservices**

Binome :
**BERRICHI Ryma**
**AKSBI Oussama**

Année : **2019/2020**

Ce fichier a été rédigé avec l'outil **markdown**

Pour visualier le preview de ce ficher read.me dans atom utilisez : [**ctrl shift m**]

Pour visualier le preview de ce ficher read.me dans Visual Studio Code utilisez : [**ctrl shift v**] ou bien [**command shift v**] si vous êtes sur Mac.



[![forthebadge](https://forthebadge.com/images/badges/made-with-javascript.svg)](https://forthebadge.com)
[![forthebadge](https://forthebadge.com/images/badges/built-with-love.svg)](https://forthebadge.com)

# Architecture choisie

Pour developper le projet, nous avons opté pour l'architecture microservices.
Pour cela, nous avons créer 3 microservices :
* Microservice pour les produits : **productsMicroservice**
* Microservice pour le stock : **stocksMicroservice**
* Microservice pour les ventes : **salesMicroservice**

Et pour gérer les différents microservices nous avons créer une **API Gateway**

Pour la base de données, nous avons opté pour des **BDD MongoDB Cloud** du site : http://cloud.mongodb.com

* Acces base de donnes des produits :

```
Adresse mail : oussama.aksbi@gmail.com
Mot de passe : ibska12341234

root
12341234
```

* Acces base de donnes des stocks :

```
Adresse mail : ryma.berrichi@gmail.com
Mot de passe : amyr12341234

root
12341234
```

* Acces base de donnes des ventes :

```
Adresse mail : aksbi.oussama@gmail.com
Mot de passe : ibska12341234

root
12341234
```


# Installations necessaires

Pour chaque microservice, faire les installations suivantes :

**NPM modules**

```
npm istall
```

**Express**

```
npm install --save express
```

**Config**

```
npm install --save config
```


**Pug**

```
npm istall --save pug
```



# Application Demo

**Produits**
* Gérés par le microservice productsMicroservice
* Pour le lancer :

```
cd productsmicroservice
node products.js
```

* Pour afficher la liste de tous les produits : http://localhost:8081

* Une fois sur cette page, on peut ajouter un nouveau produit en cliquant sur le bouton [**New Product**] en haut de la page

* Sur cette même page, on peut augmenter la quantité d'un produit en stock. Pour le faire, on remplit le champs [**Quantity to addd...**] avec la quantité qu'on désire ajouter (à ce qui existe déja) et on finalise l'action en cliquant sur le bouton [**Add to stock**]

* Pour vérifier que la quantité a augmenté, on peut vérifier sur la page d'accueil du microservice des stocks (2ème partie)

* En plus, on peut voir le detail de chaque produit en cliquant sur son nom.

* Une fois sur la page de détails, on peut soit le supprimer avec le bouton [**Delete**] ou le mettre à jour/modifier avec le bouton [**Edit**]


**Stocks**
* Gérés par le microservice stocksMicroservice
* Pour le lancer :

```
cd stocksmicroservice
node stocks.js
```

* Pour afficher la liste de tous les produits en stock avec la quantité disponible : http://localhost:8083

* De la même maniére, on peut diminuer la quantité d'un produit en stock. Pour le faire, on remplit le champs [**Quantity to remove...**] avec la quantité qu'on désire supprimer/diminuer  et on finalise l'action en cliquant sur le bouton [**Remove from stock**].

* Le changement de quantité sera visible directement sur la meême page ( grâce à **AJAX** )



**Ventes**
* Gérés par le microservice salesmicroservice
* Pour le lancer :

```
cd salesmicroservice
node sales.js
```

* Pour afficher la liste de toutes les ventes : http://localhost:8082

* Une fois sur cette page, on peut créer une nouvelle vente en cliquant sur le bouton [**New Sale**] en haut de la page.

* En plus, on peut voir le detail de chaque vente en cliquant sur son nom.


**Etats des microservices**
* Pour visualiser l'état de tous les microservices en temps réel (UP/DOWN): http://localhost:8088

**Redirection**
* Si tous les microservices sont lancés (UP), on peut passer d'un microservice à un autre grave à la barre de navigation en haut de la page.

# Infos utiles

Pour utiliser le projet dans son intégralité, il faut lancer les 3 microservices ainsi que l'API Gateway :

Feature | Type | Route
------------ | ------------- | -------------
Afficher l'etat des microservices | GET | http://localhost:8088
Afficher tout les produits | GET | http://localhost:8081
Ajouter un nouveau produit | POST | http://localhost:8081/products/new
Mettre à jour un produit | PUT | http://localhost:8081/products/edit/:id
Afficher l'etat du stock | GET | http://localhost:8083
Afficher les ventes | GET | http://localhost:8082
Ajouter une vente | POST | http://localhost:8082/sales/new
