// load express
  const express = require('express');
  const app = express();
  const bodyParser = require('body-parser');
  var path = require('path');

  var config = require('config');
  var microserviceConfig = config.get('microservice.config');


  app.engine('pug', require('pug').__express)
  app.set('views', path.join(__dirname, 'views'));
  app.set('view engine', 'pug');

  app.use(express.static('static'));


  app.use(bodyParser.urlencoded({ extended: false }))
  app.use(bodyParser.json());

  // load mongoose
  const mongoose = require('mongoose');

  // load model Stock
  require('./models/saleModel')
  const Sale = mongoose.model('Sale');


// connect to database
  mongoose.connect('mongodb+srv://root:12341234@salesservice-y2uly.mongodb.net/test?retryWrites=true&w=majority', () =>{
    console.log('Sales database connected!');
  });



 // main page
  app.get('/', function(req,res){
  Sale.find({}, function(err, sales){
  //  let ssales = sales;
      res.render('index', {
        title: 'Sales',
        sales: sales
      });
    });

});



  // list all the sales in json
  app.get('/getSales', (req, res) => {
    Sale.find().then((sales) => {
      console.log(sales);
      res.json(sales)
    }).catch(err => {
      if(err) {
        throw err;
      }
    });
  });



  // create a new sale
  app.post('/sale', (req,res) => {

    // transforming post request to json object

    var newSale = {
      productId: req.body.productId,
      prixTotal: 20,
      qt: req.body.productId,
    }

    console.log(newSale);

    // create a new sale instance
    var sale = new Sale(newSale);

    sale.save().then(() => {
      console.log('New sale created.')
    }).catch((err) => {
      if(err) {
        throw err;
      }
    })
    res.send('New sale created!');
  });



  // Add sale route


  // Add sale submit post route



/*
  app.post('/sales/new', function(req, res) {
  let sale = new Sale();
  sale.productId = req.body.productId;
  sale.prixTotal = 20
  sale.qt = req.body.qt,

  console.log(req.body);
  sale.save(function(err) {
    if (err) {
      console.log(err);
      return;
    } else {
      res.redirect('/');
    }
  });
});  */



app.post('/sales/new', function(req, res) {
  console.log('salesnew');
  let sale = new Sale();
  sale.productId = req.body.productId;
  sale.productName = req.body.productName;
  sale.qt = req.body.qt;
  sale.price = req.body.price;
  sale.totalAmount = req.body.totalAmount;
  //console.log(req.body);

  console.log(sale.productId);
  console.log('new sale');
  if (Math.sign(sale.qt) != -1 && !isNaN(Math.sign(sale.qt))) {

      sale.save(function(err) {
      if (err) {
        console.log(err);
        return;
      } else {
        res.redirect('/');
      }

    });
  }





});






  // list one single sale
  app.get('/sale/:id', function(req,res){
    Sale.findById(req.params.id, function(err, sale){
      res.render('sale', {
        sale: sale

      });
    });

  });











  app.get('/heartbeat', function(req, res){
  	var status = {
  		success: true,
  		address: server.address().address,
  		port: server.address().port
  	 };
  	res.send(status);
  });



  var server = app.listen(microserviceConfig.port,  microserviceConfig.host, function () {
  	var host = server.address().address;
  	var port = server.address().port;

  	console.log('Server running on: http://%s:%s', host, port);
  });
