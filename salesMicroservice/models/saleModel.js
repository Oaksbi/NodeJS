// load mongoose
  const mongoose = require('mongoose');

mongoose.model('Sale', {

  productId: {
    type: String,
    //type: mongoose.SchemaTypes.ObjectId,
    required: true
  },
  productName: {
    type: String,
    require: true
  },
  qt: {
    type: Number,
    require: true
  },
  price: {
    type: Number,
    required: true
  },
  totalAmount: {
    type: Number,
    required: true
  }

});
