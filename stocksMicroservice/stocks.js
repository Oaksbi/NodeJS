// load express
  const express = require('express');
  const app = express();
  const bodyParser = require('body-parser');
  var path = require('path');

  var config = require('config');
  var microserviceConfig = config.get('microservice.config');


  app.engine('pug', require('pug').__express)
  app.set('views', path.join(__dirname, 'views'));
  app.set('view engine', 'pug');

  app.use(express.static('static'));


  app.use(bodyParser.urlencoded({ extended: false }))
  app.use(bodyParser.json());

  // load mongoose
  const mongoose = require('mongoose');

  // load model Stock
  require('./models/stockModel')
  const Stock = mongoose.model('Stock');



  // connect to the database
  mongoose.connect('mongodb+srv://root:12341234@stocksservice-msm2y.mongodb.net/test?retryWrites=true&w=majority', () =>{
    console.log('Stocks database connected!');
    });



  app.post('/stock', (req,res) => {

  });



  // list all the products in json
  app.get('/getStocks', (req, res) => {
    Stock.find().then((stocks) => {
      console.log(stocks);
      res.json(stocks)
    }).catch(err => {
      if(err) {
        throw err;
      }
    });
  });






  app.get('/', function(req,res){
  Stock.find({}, function(err, stocks){

      res.render('index', {
        title: 'Stocks',
        stocks: stocks
      });
    });

});


app.post('/stocks/new', function(req, res) {
  let stock = new Stock();
  stock.id = req.body.id;
  stock.name = req.body.name;
  stock.category = req.body.category;
  stock.price = req.body.price;
  stock.qt = req.body.qt;
  //console.log(req.body);

  console.log(stock.id);

  Stock.find({id:stock.id}, function(err,stocks){
    if (stocks.length == 0) {
      console.log('doesnt exist');
      if (Math.sign(stock.qt) != -1 && !isNaN(Math.sign(stock.qt))) {

          stock.save(function(err) {
          if (err) {
            console.log(err);
            return;
          } else {
            res.redirect('/');
          }

        });
      }
    } else {
      console.log('already exists');
      if (Math.sign(stock.qt) != -1 && !isNaN(Math.sign(stock.qt))) {

        let stockUpdated = {};
        oldQt = stocks[0].qt;
        newQt = stock.qt;
        if (newQt == null) {
          newQt = 0;
        }
        if (oldQt == null) {
          oldQt = 0;
        }
        stockUpdated.qt = oldQt + newQt;

        Stock.update({id:stock.id}, stockUpdated, function(err) {

          if (err) {
            console.log(err);
            return;
          } else {
            res.redirect('/');
          }

        });
      }
    }

  });


});




  // update a stock
  app.put('/stock/:id', (req, res) => {

    let stock = {};
    stock.qt = req.body.qt;

    let query= {_id:req.params.id}

    Stock.update(query, stock, function(err) {
      if (err) {
        console.log(err);
        return;
      } else {
        res.redirect('/');
      }

    });

  });


  app.get('/heartbeat', function(req, res){
  	var status = {
  		success: true,
  		address: server.address().address,
  		port: server.address().port
  	 };
  	res.send(status);
  });



  var server = app.listen(microserviceConfig.port,  microserviceConfig.host, function () {
  	var host = server.address().address;
  	var port = server.address().port;

  	console.log('Server running on: http://%s:%s', host, port);
  });
