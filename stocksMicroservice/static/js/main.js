$(document).ready(function() {
  $('.removeStock').on('click', function(e){
    $target = $(e.target);
    const id = $target.attr('data-id');
    const qt = $target.attr('data-qt');
    const qtRem = document.getElementById('quantity'+id).value;
    var quantity = qt - qtRem;

    if (qt <= 0) {
      quantity = 0;
    }

    $.ajax({
      type: 'put',
      url: '/stock/'+id,
      success: function(response){
        location.reload(true);

      },
      data: {"qt": quantity},
      error: function(err){
        location.reload(true);
        console.log(err);
      }

    });
  });
});
