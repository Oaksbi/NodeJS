
// load mongoose
  const mongoose = require('mongoose');

mongoose.model('Stock', {
  // Id, Date, Qt

  id: {
    type: String,
    require: true
  },
  name: {
    type: String,
    require: true
  },
  category: {
    type: String,
    require: true
  },
  price: {
    type: Number,
    require: true
  },
  qt: {
    type: Number,
    require: true
  }

});
