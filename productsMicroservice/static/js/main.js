
$(document).ready(function() {
  $('.DeleteProduct').on('click', function(e){
    $target = $(e.target);
    const id = $target.attr('data-id');

    $.ajax({
      type: 'DELETE',
      url: '/product/'+id,
      success: function(response){
        window.location.href='/';
      },
      error: function(err){
        console.log(err);
      }

    });
  });
});



$(document).ready(function() {
  $('.addStock').on('click', function(e){
    $target = $(e.target);
    const id = $target.attr('product-id');
    const name = $target.attr('product-name');
    const category = $target.attr('product-category');
    const price = $target.attr('product-price');
    const qt = document.getElementById('quantity'+id).value;
    var xhr = new XMLHttpRequest();
    var url = "http://localhost:8083/stocks/new";

    xhr.open("POST", url, true);
    xhr.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');

    xhr.onreadystatechange = function() {
        if(xhr.readyState == 4 && xhr.status == 200) {
            alert(xhr.responseText);
        }
    }
    var data = 'id='+id+'&name='+name+'&Category='+category+'&price='+price+'&qt='+qt;
    xhr.send(data);



  });
});


$(document).ready(function() {
  $('.addSale').on('click', function(e){
    $target = $(e.target);
    const productId = $target.attr('product-id');
    const productName = $target.attr('product-name');
    const price = $target.attr('product-price');
    const qt = document.getElementById('quantity'+productId).value;
    const totalAmount = qt*price;

    var xhr = new XMLHttpRequest();
    var url = "http://localhost:8082/sales/new";

    xhr.open("POST", url, true);
    xhr.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');

    xhr.onreadystatechange = function() {
        if(xhr.readyState == 4 && xhr.status == 200) {
            alert(xhr.responseText);
        }
    }
    var data = 'productId='+productId+'&productName='+productName+'&price='+price+'&qt='+qt+'&totalAmount='+totalAmount;
    xhr.send(data);



  });
});
