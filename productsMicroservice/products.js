// load express
  const express = require('express');
  const app = express();
  const bodyParser = require('body-parser');
  var path = require('path');

  var config = require('config');
  var microserviceConfig = config.get('microservice.config');

app.engine('pug', require('pug').__express)
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

app.use(express.static('static'));



  app.use(bodyParser.urlencoded({ extended: false }))
  app.use(bodyParser.json());

// load mongoose
  const mongoose = require('mongoose');

// load model Product
  require('./models/productModel')
  const Product = mongoose.model('Product');

// connect to the database

mongoose.connect('mongodb+srv://root:12341234@productsservice-prnzb.mongodb.net/test?retryWrites=true&w=majority', () =>{
  console.log('Products database connected!');
});



// create a new product
app.post('/product', (req,res) => {

  // transforming post request to json object

  var newProduct = {
    id: req.body.id,
    name: req.body.name,
    category: req.body.category,
    price: req.body.category
  }

  console.log(newProduct);

  // create a new product instance
  var product = new Product(newProduct);

  product.save().then(() => {
    console.log('New product created.')
  }).catch((err) => {
    if(err) {
      throw err;
    }
  })
  res.send('New product created!');
})


// list all the products in json
app.get('/getProducts', (req, res) => {
  Product.find().then((products) => {
    console.log(products);
    res.json(products)
  }).catch(err => {
    if(err) {
      throw err;
    }
  })
})

// list one single product
app.get('/product/:id', function(req,res){
  Product.findById(req.params.id, function(err, product){
    res.render('product', {
      product: product
    });
  });
});


// Edit a product - load edit form
app.get('/product/edit/:id', function(req,res){
  Product.findById(req.params.id, function(err, product){
    res.render('editProduct', {
      title: 'Edit the product',
      product: product
    });
  });
});

// delete a product
app.delete('/product/:id', (req, res) => {
  Product.findByIdAndRemove(req.params.id).then(() => {
    res.send('Product Removed!');
  }).catch(err => {
    if(err) {
      throw err;
    }
  })
})




app.get('/', function(req,res){
  Product.find({}, function(err, produits){
    let products = produits;
    res.render('index', {
      title: 'Products',
      products: products
    });
  });

});


// Add product route

app.get('/products/new', function(req, res) {
  res.render('newProduct', {
    title: 'Add a new product'
  });

});

// Add product submit post route

app.post('/products/new', function(req, res) {
  let product = new Product();
  product.id = req.body.id;
  product.name = req.body.name;
  product.category = req.body.category;
  product.price = req.body.price;

  console.log(req.body);
  product.save(function(err) {
    if (err) {
      console.log(err);
      return;
    } else {
      res.redirect('/');
    }

  });

});



// Update product submit post route
app.post('/products/edit/:id', function(req, res) {
  let product = {};
  product.id = req.body.id;
  product.name = req.body.name;
  product.category = req.body.category;
  product.price = req.body.price;

  let query= {_id:req.params.id}

  console.log(req.body);
  Product.update(query, product, function(err) {
    if (err) {
      console.log(err);
      return;
    } else {
      res.redirect('/');
    }

  });
});


// Delete product
  app.delete('/product/:id', function(req, res){
    let query = { _id:req.params.id}
    product.remove(query, function(err) {
      if (err){
        console.log(err);
      }
      res.send('deleted with success');
    });
  });


app.get('/heartbeat', function(req, res){
	var status = {
		success: true,
		address: server.address().address,
		port: server.address().port
	 };
	res.send(status);
});




var server = app.listen(microserviceConfig.port,  microserviceConfig.host, function () {
	var host = server.address().address;
	var port = server.address().port;

	console.log('Server running on: http://%s:%s', host, port);
});
